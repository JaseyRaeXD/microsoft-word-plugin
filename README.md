# README #

### What is this repository for? ###

This was a plugin that connected to a companies major product (A) that allowed a user to search content within A and pull them into word. This allowed users to edit content through a familiar service (Word) and then send it back to A for further review.

Version 3

### How do I get set up? ###

*Ready for view in small screen sizes or mobile. It is meant to be displayed on desktop/laptop with width: 400px. (Since it is a Word sidebar plugin)