$(document).ready(function() {

    $('#comp').click(function(){
        $('.component-type-area').removeClass('hidden');  
    });

    $('#doc').click(function(){
        $('.component-type-area').addClass('hidden');      
    });
    
    //this is very unusable bc of the delay, how can i make these calculations/page load faster?
    setTimeout(function(){
        //this aligns the accept and reject links for each revision item
        $('.rev-actions').each(function(){
            //panel height
            var ph = $(this).parent().parent().children('.col-xs-9').children('.panel').height();
            console.log(ph);
            //rev actions height
            var rah = $(this).height();
            console.log(rah);
            //required margin top
            var mt = (ph - rah) / 2;
            $(this).css("margin-top", mt);
            console.log(mt);    
        });   
        
    },800);

    $('.toggle-footer i').click(function(){
        $(this).toggleClass('ion-chevron-up ion-chevron-down'); 
        $('.footer').toggleClass('footer-sm');
        $('.footer-action-btn').toggleClass('footer-action-btn-sm');
        
    });
    
    
});

